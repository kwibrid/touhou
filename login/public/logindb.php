<?php

//数据库初始化操作文件

//连接认证
header('Content-type:text/html ; charset=utf-8');
$dbms = 'mysql';
$host = 'localhost';
$dbname = 'kwibrid';
$user = 'root';
$pass = '';
$dsn = "$dbms:host = $host;dbname = $dbname";

function my_error($dsn,$user,$pass,$sql){
    $pdo = new PDO($dsn,$user,$pass,array(PDO::MYSQL_ATTR_INIT_COMMAND => "set names utf8"));
    try {
        $res = $pdo->query($sql);
    } catch(PDOException $ex) {
        echo '出现异常：<br/>';
        echo '错误出现的位置：' . $ex->getFile() . $ex->getLine() . '<br/>';
        echo '错误原因：'  . $ex->getMessage();

        var_dump($ex->getTrace());
        exit;
    }
    return $res;
}


$sql = "use kwibrid.userinfo";
my_error($dsn,$user,$pass,$sql);